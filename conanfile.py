import os

from conans import ConanFile, tools


class Sol2Conan(ConanFile):
    name           = "sol2"
    version        = "3.0.3"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-sol2"
    description    = "a C++ <-> Lua API wrapper with advanced features and top notch performance - is here, and it's great!"
    topics         = ("lua", "wrapper")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        tools.download("https://github.com/ThePhD/sol2/releases/download/v{}/sol.hpp".format(self.version), "sol/sol.hpp")
        tools.download("https://github.com/ThePhD/sol2/releases/download/v{}/forward.hpp".format(self.version), "sol/forward.hpp")

        # self.run("git clone ...") or
        # tools.download("url", "file.zip")
        # tools.unzip("file.zip" )

    def package(self):
        self.copy("*.hpp", "include")
